import getpass
import sys
import os
import subprocess

import yaml
from types import SimpleNamespace


configuration = {}
MODULE_NAME = 'helmy'
MODULE_VERSION = '0.3.0'
MODULE_DESCRIPTION = 'Module to interact with helm charts'
CONFIG_SECTION = MODULE_NAME.lower()
COMMANDS = ['lint', 'lint-ignore', 'create', 'explain', 'version', 'help']
COMMAND_HELP = {
    'lint': 'lint a helm chart for missing values or fields',
    'lint-ignore': 'create a helmy.yaml to start ignoring some checks',
    'create': 'create a new helm chart, interactively',
    'explain': 'explain the current helm chart',
    'version': 'show module version',
    'help': 'show help information'
}

def execute(command, params, app_config):
    global configuration
    configuration = app_config
    if command == 'lint':
        _helmy_lint()
    elif command == 'lint-ignore':
        _helmy_lint_ignore()
    elif command == 'create':
        _helmy_create()
    elif command == 'explain':
        _helmy_explain()
    elif command == 'version':
        _helmy_version()
    else:
        _helmy_help()



def _helmy_lint():
    global configuration
    module_config = configuration.get(CONFIG_SECTION)
    if _helmy_in_helm_chart():
        chart_data = _helmy_load_yaml_from_file('Chart.yaml')
        print(
            f'Current helm chart: {chart_data.get("name")} - {chart_data.get("version")}')
        # needs helm to work!
        try:
            # call helm lint
            helm_lint_process = subprocess.run(
                ['helm', 'lint'], capture_output=True, check=True)
            print('1. Linting helm charts...\n')
            for line in helm_lint_process.stdout.decode().split('\n'):
                if '[' in line:
                    print('-',line)
            # call helm template and check results
            print('\n2. Checking helm charts for missing or invalid fields...')
            helm_template_process = subprocess.run(
                ['helm', 'template', '.'], capture_output=True, check=True)
            manifests = _helmy_load_yaml_from_string(
                helm_template_process.stdout.decode())
            for manifest in manifests:
                overrides = _helmy_get_overrides()
                _helmy_check(manifest, overrides)

        except HelmyMissingFieldException as e:
            print(e)
            sys.exit(1)
        except FileNotFoundError:
            print('ERROR: "helm" not installed')
            sys.exit(1)
        except subprocess.CalledProcessError as e:
            print(
                f'Process returned non-zero exit code, message was:\n {e.output.decode()}')

        print('\nDone, no errors')


def _helmy_lint_ignore():
    with open('helmy.yaml', 'w') as helmy_file:
        helmy_file.write("""# helmy default lint configuration, this is the default configuration, used by helmy

all:
  maintainer: False  # ignore missing metadata.labels.maintainer field
services: 
  ports: False  # ignore missing ports for service objects
  type: False  # ignore warning for Loadbalancer type service objects
deployments:
  selector: False  # ignore missing spec.selector.matchLabels in spec.template.spec.metadata.labels
  container: False  # ignore missing containers
  name: False  # ignore missing spec.template.spec.containers.*.name field
  image: False  # ignore missing spec.template.spec.containers.*.image field
  resources: False  # ignore missing resource requests/limits block
ingress:
  annotations: False  # ignore missing annotations block
  clusterIssuer: False  # ignore missing cert-manager.io/cluster-issuer annotation
  class: False  # ignore missing kubernetes.io/ingress.class annotation
  hostMismatch: False  # ignore mismatch between rules.hosts and tls.hosts
""")
    print('Done, created helmy.yaml')

def _helmy_create():
    global configuration
    module_config = configuration.get(CONFIG_SECTION)
    # interactively, ask for:
    # - chartname (base for filenames)
    # - imageName?
    # - port?
    # - service needed?
    # - ingress needed?
    # - ingress SSL ?
    # - ingress DOMAIN ?
    # - ingress CORS ?


def _helmy_explain():
    global configuration
    module_config = configuration.get(CONFIG_SECTION)
    # basic explanation only. Which resources will be deployed, best with names


def _helmy_version():
    print(f'helmy ccli submodule - version {MODULE_VERSION}')


def _helmy_help():
    print(f"""Usage:
    {sys.argv[0]} helmy [COMMAND]

The following commands are available
{'lint':10} - Lint the helm chart in the current directory.
{'create':10} - Interactively create a helm chart
{'explain':10} - Get an explanation what the current helm chart does
{'version':10} - Show module version.
{'help':10} - Show this help information.
"""
          )

class HelmyMissingFieldException(Exception):
    pass

def _helmy_in_helm_chart():
    if not os.path.exists('Chart.yaml'):
        print('found no Chart.yaml')
        sys.exit(1)
    return True

def _helmy_get_overrides():
    overrides = {
        'all': {
            'maintainer': False,
        },
        'services': {
            'ports': False,
            'type': False,
        },
        'deployments': {
            'selector': False,
            'container': False,
            'name': False,
            'image': False,
            'resources': False,
        },
        'ingress': {
            'annotations': False,
            'clusterIssuer': False,
            'class': False,
            'hostMismatch': False,
        },
    }
    if os.path.exists('helmy.yaml'):
        try:
            overrides_from_file = _helmy_load_yaml_from_file('helmy.yaml')
            overrides.update(overrides_from_file)
        except TypeError:
            print('- [WARNING] - helmy.yaml is empty, or broken, falling back to defaults')
    overrides = SimpleNamespace(**overrides)
    return overrides

def _helmy_load_yaml_from_string(string):
    try:
        yaml_data = yaml.load_all(string, Loader=yaml.FullLoader)
    except yaml.YAMLError as exception:
        print(exception)
        sys.exit(1)
    return yaml_data


def _helmy_load_yaml_from_file(path):
    with open(path, 'r') as stream:
        try:
            yaml_data = yaml.safe_load(stream)
        except yaml.YAMLError as exception:
            print(exception)
            sys.exit(1)
    return yaml_data


def _helmy_get_label_overrides(labels, overrides):
    for label in labels:
        if 'helmy.hmg.systems/ignore' in label:
            category = label.split('-')[1].split('.')[0]
            field = label.split('-')[1].split('.')[1]
            new_category = vars(overrides).get(category)
            new_category[field] = labels.get(label)
            setattr(overrides, category, new_category)
    return overrides







def _helmy_check(manifest, overrides):
    kind = manifest.get('kind').lower()
    meta = manifest.get('metadata')
    if not meta:
        raise HelmyMissingFieldException('- [ERROR] - No metadata found!')
    name = meta.get('name')
    print('\nFound', manifest.get('kind'), 'named', name)
    labels = meta.get('labels')
    if not labels:
        raise HelmyMissingFieldException('- [ERROR] - No labels found!')
    overrides = _helmy_get_label_overrides(labels, overrides)
    maintainer = labels.get('maintainer')
    spec = manifest.get('spec')

    # maintainer needs to be set for all types
    if not maintainer and not overrides.all.get('maintainer'):
        raise HelmyMissingFieldException('- [ERROR] - Found no maintainer')
    if kind == 'service':
        service_ports = spec.get('ports')

        # first check: ports should be set
        if not service_ports or len(service_ports) == 0 and not overrides.services.get('ports'):
            raise HelmyMissingFieldException('- [ERROR] - No ports specified')

        # give a warning when service is type loadbalancer
        if spec.get('type') and spec.get('type').lower() == 'loadbalancer' and not overrides.services.get('type'):
            print('- [WARNING] - Service is type "Loadbalancer", this will create a PublicIP and expose this service!')
    
    if kind in ['deployment', 'statefulset', 'daemonset']:
        pod_template = spec.get('template')
        pod_labels = pod_template.get('metadata').get('labels')
        pod_spec = pod_template.get('spec')
        # first check: spec.selector.matchLabels in spec.template.metadata.labels
        selectorLabels = spec.get('selector').get('matchLabels')
        if not all(item in pod_labels for item in selectorLabels) and not overrides.deployments.get('selector'):
            raise HelmyMissingFieldException('- [ERROR] - spec.selector.matchLabels are not in spec.template.metadata.labels! This will lead to a deployment with no matching pods')

        # second check: spec.template.spec.containers len > 0
        if len(pod_spec.get('containers')) == 0 and not overrides.deployments.get('container'):
            raise HelmyMissingFieldException('- [ERROR] - No containers specified')

        # third check: all containers have image, name, resources set
        for container in pod_spec.get('containers'):

            if not container.get('name') and not overrides.deployments.get('name'):
                raise HelmyMissingFieldException('- [ERROR] - No container name set')

            print(f'Checking container {name}')

            if not container.get('image') and not overrides.deployments.get('image'):
                raise HelmyMissingFieldException(f'- [ERROR] - No image set for container {name}')

            if not container.get('resources') and not overrides.deployments.get('resources'):
                raise HelmyMissingFieldException(f'- [ERROR] - No resources set for container {name}')

            if not all(item in container.get('resources').get('limits') for item in ['memory', 'cpu']) and not overrides.deployments.get('limits'):
                raise HelmyMissingFieldException(f'- [ERROR] - Missing memory/cpu limits for container {name}')

            if not all(item in container.get('resources').get('requests') for item in ['memory', 'cpu']) and not overrides.deployments.get('requests'):
                raise HelmyMissingFieldException(f'- [ERROR] - Missing memory/cpu requests for container {name}')
        
    if kind == 'ingress':
        annotations = meta.get('annotations')

        # first check: annotation block must be present
        if not annotations and not overrides.ingress.get('annotations'):
            raise HelmyMissingFieldException('- [ERROR] - No annotations found')

        # second check: ingress class should be nginx
        if not annotations.get('kubernetes.io/ingress.class') == 'nginx' and not overrides.ingress.get('class'):
            raise HelmyMissingFieldException('- [ERROR] - kubernets.io/ingress.class is not set to "nginx"')

        # second check: cluster-issuer is set
        if not annotations.get('cert-manager.io/cluster-issuer') and not overrides.ingress.get('clusterIssuer'):
            raise HelmyMissingFieldException('- [ERROR] - No cluster issuer set')

        # third check: hosts in tls and rules block are the same
        rules_hosts = [rule.get('host') for rule in spec.get('rules')]
        tls_hosts = []
        for tls_block in spec.get('tls'):
            [tls_hosts.append(host) for host in tls_block.get('hosts')]
        if not tls_hosts in rules_hosts and not overrides.ingress.get('hostMismatch'):
            raise HelmyMissingFieldException('- [ERROR] - No host rule for every tls host set up')


def _helmy_create():
    # step 1: Ask for basic information
    chart_name = input(f'1. Chart name > ')
    maintainer = input('2. Maintainer > ')

    # step 2: Create base folder structure
    for folder in [chart_name, f'{chart_name}/charts', f'{chart_name}/templates']:
        if not os.path.exists(folder):
            os.mkdir(folder)

    values_yaml = {
        'instanceName': chart_name,
        'maintainer': maintainer,
        'deployments':{},
        'services':{},
        'ingress':{}
    }
    

    chart_unfinished = True
    while chart_unfinished:
        command = input("""3. Object creation, inputs are not case-sensitive! Possible Values:
        - Deployment / dep
        - Service / svc
        - Ingress / ing
What would you like to add? Type "done" or leave empty to finish your helm chart > """)
        if command.lower() == 'deployment' or command.lower() == 'dep':
            _helmy_create_deployment(chart_name, maintainer, values_yaml)
        elif command.lower() == 'service' or command.lower() == 'svc':
            _helmy_create_service(chart_name, maintainer, values_yaml)
        elif command.lower() == 'ingress' or command.lower() == 'ing':
            _helmy_create_ingress(chart_name, maintainer, values_yaml)
        elif command.lower() == 'done' or command.lower() == "":
            chart_unfinished = False
    _helmy_create_finish_chart(chart_name, maintainer, values_yaml)

def _helmy_create_ingress(chart_name, maintainer, values_yaml):
    ing_name = input('Ingress name > ')
    ing_internal_name = _helmy_convert_to_internal_name(ing_name)
    ing_clusterissuer = input('Name of the ClusterIssuer? (default: letsencrypt-prod) > ')
    ing_secret_name = f'{chart_name}-{ing_name}-cert'
    ing_hosts = []
    if ing_clusterissuer == '':
        ing_clusterissuer = 'letsencrypt_prod'
    if input('Add custom labels? (y|N) >').lower() in ['y', 'yes']:
        custom_labels = _helmy_add_custom_labels()
    else:
        custom_labels = {'helmy.hmg.systems/generated': 'yes'}
    values_yaml['ingress'][ing_internal_name] = {
        'clusterIssuer': ing_clusterissuer,
        'secretName': ing_secret_name,
        'hosts':{}
    }
    while input('Add a new host to this ingress? (y|N) > ').lower() not in ['n', '']:
        host = {}
        host['id'] = f'{ing_internal_name}Host{len(ing_hosts)}'
        host['dns'] = input('Address or Hostname > ')
        host['service'] = input('Name of the target Service object > ')
        host['port'] = input('Port of the target Service object > ')
        ing_hosts.append(host)
        for host in ing_hosts:
            print(f'\n{"HOST ID":25}|{"DNS":50}|{"SERVICE:PORT":25}')
            print(f'{"-"*25}|{"-"*50}|{"-"*25}')
            print(f'{host["id"]:25}|{host["dns"]:50}|{host["service"] + ":" + host["port"]:25}\n')
            values_yaml['ingress'][ing_internal_name]['hosts'][host['id']] = {
                'dns': host['dns'],
                'service': host['service'],
                'port': host['port']
            }

    with open(f'{chart_name}/templates/ing-{ing_name}.yaml', 'w') as ingress_file:
        ingress_file.write(f"""# helmy-generated ingress for {ing_name}
    {'{{ include "hmg.api.Ingress" . }}'}
kind: Ingress
metadata:
  labels:
    {'{{include "hmg.labels" . | indent 4}}'}
""")
        for label in custom_labels:
            ingress_file.write(f'    {label}: {custom_labels.get(label)}\n')
        ingress_file.write(f"""    maintainer: "{maintainer}"
  name: "{ing_name}-ing"
  annotations:
  {'{{- include "hmg.ingressAnnotations" . | indent 4 }}'}
    cert-manager.io/cluster-issuer: {"{{.Values.ingress." + ing_internal_name + ".clusterIssuer}}"}
spec:
  rules:
""")
        for host in ing_hosts:
            ingress_file.write(f"""
    - host: {'{{.Values.ingress.' + ing_internal_name + '.hosts.' + host['id'] + '.dns}}'}
      http:
        paths:
            - backend:
                serviceName: {'{{.Values.ingress.' + ing_internal_name + '.hosts.' + host['id'] + '.service}}'}
                servicePort: {'{{.Values.ingress.' + ing_internal_name + '.hosts.' + host['id'] + '.port}}'}
""")
        ingress_file.write(f"""
  tls:
    - hosts:
""")
        for host in ing_hosts:
            ingress_file.write(f'        - {"{{.Values.ingress." + ing_internal_name + ".hosts." + host["id"] + ".dns}}"}\n')
        ingress_file.write(f'      secretName: {"{{.Values.ingress." + ing_internal_name + ".secretName}}"}')

def _helmy_convert_to_internal_name(name):
    for symbol in ['-', '_', '.']:
        if symbol in name:
            internal_name_parts = name.split(symbol)
            name = internal_name_parts[0] + ''.join([part.title() for part in internal_name_parts[1:]])
    return name






def _helmy_create_service(chart_name, maintainer, values_yaml):
    svc_name = input('Service name > ')
    if input('Add custom labels? (y|N) >').lower() in ['y', 'yes']:
        custom_labels = _helmy_add_custom_labels()
    else:
        custom_labels = {'helmy.hmg.systems/generated': 'yes'}
    service_type= input("""Enter the Service type. Input is not case-sensitive. Possible Values:
    Loadbalancer - Receives an external IP and is exposed to the public
    ClusterIP    - (default) Behaves like the Loadbalancer type, except only for cluster-internal communication
    ExternalName - Points to a DNS instead of using labels to select pods. Can also point to other Service objects
Select one to continue, leaving the field empty results in "ClusterIP" being chosen. > """).lower()
    service_source_port = input(f'Service inbound port > ')
    service_target_port = input(f'Service target port > ')
    if input(f'Enable SessionAffinity? (y|N) > ') in ['y', 'yes']:
        service_session_affinity = 'ClientIP'
    else:
        service_session_affinity = 'None'
    values_yaml['services'][svc_name] = {
        'sessionAffinity': service_session_affinity,
        'inboundPort': service_source_port,
        'targetPort': service_target_port
    }
    if service_type != 'externalname': 
        selector_name = input('Enter a unique label name to be used as a selector for your pods > ')
        selector_value = input('Enter a value for the unique selector label > ')
        svc_template_selectors = { 'chart': chart_name, selector_name: selector_value }
    else:
        external_name = input('Enter DNS to point your Service to > ')
        values_yaml['services'][svc_name]['externalName'] = external_name

    with open(f'{chart_name}/templates/svc-{svc_name}.yaml', 'w') as service_file:
        service_file.write(f"""# helmy-generated service for {svc_name}
    {'{{ include "hmg.api.Service" . }}'}
kind: Service
metadata:
  labels:
    {'{{include "hmg.labels" . | indent 4}}'}
""")
        for label in custom_labels:
            service_file.write(f'    {label}: {custom_labels.get(label)}\n')
        service_file.write(f"""    maintainer: "{maintainer}"
  name: "{svc_name}-svc"
spec:
  ports:
    - port: "{"{{ .Values.services." + svc_name + ".inboundPort}}"}"
      targetPort: "{"{{ .Values.services." + svc_name + ".targetPort}}"}"
      name: default-port
""")
        if service_type != 'externalname':
            service_file.write(f"""
  selector:
""")
            for selector in svc_template_selectors:
                service_file.write(f'    {selector}: {svc_template_selectors.get(selector)}\n')
        else:
            service_file.write(f'  externalName: "{"{{ .Values.services." + svc_name + ".externalName}}"}"\n')
        service_file.write(f"""
  sessionAffinity: "{"{{ .Values.services." + svc_name + ".sessionAffinity}}"}"
  type: {service_type}
""")

def _helmy_create_deployment(chart_name, maintainer, values_yaml):
    dep_name = input('Deployment name > ')
    
    if input('Add custom labels? (y|N) >').lower() in ['y', 'yes']:
        custom_labels = _helmy_add_custom_labels()
    else:
        custom_labels = {'helmy.hmg.systems/generated': 'yes'}
    selector_name = input('Enter a unique label name to be used as a selector for your pods > ')
    selector_value = input('Enter a value for the unique selector label > ')
    dep_template_selectors = { 'chart': chart_name, selector_name: selector_value }
    try:
        dep_replicas = int(input('Amount of replicas > '))
    except ValueError:
        print('No integer value detected, continuing with 1 replica/s')
        dep_replicas = 1
    values_yaml['deployments'][dep_name] = {
        'replicaCount': dep_replicas
    }
    try:
        dep_containers = int(input('Amount of Containers > '))
    except ValueError:
        print('No integer value detected, continuing with 1 container/s')
        dep_containers = 1
    containers = []
    values_yaml['deployments'][dep_name]['containers'] = {}
    for i in range(0,dep_containers):
        container = {}
        container['name'] = input(f'Container #{i} name > ')
        container['image'] = input(f'Container #{i} / {container["name"]} docker image > ')
        container['port'] = input(f'Container #{i} / {container["name"]} application port > ')
        container['imageTag'] = 'latest'
        container['resources'] = {'limits':{
            'cpu': input(f'Container #{i} / {container["name"]} cpu limit > '),
            'memory': input(f'Container #{i} / {container["name"]} memory limit > ')
        },'requests':{
            'cpu': input(f'Container #{i} / {container["name"]} minimum amount of cpu > '),
            'memory': input(f'Container #{i} / {container["name"]} minimum amount of memory > ')
        }}
        containers.append(container)
        values_yaml['deployments'][dep_name]['containers'][container['name']] = {
            'port': container['port'],
            'image': container['image'],
            'imageTag': container['imageTag'],
            'resources': container['resources']
        }
    with open(f'{chart_name}/templates/dep-{dep_name}.yaml', 'w') as deployment_file:
        deployment_file.write(f'# helmy-generated deployment for {dep_name}\n')
        deployment_file.write("""{{ include "hmg.api.Deployment" . }}
kind: Deployment
metadata:
  labels:
    {{include "hmg.labels" . | indent 4}}
""")
        for label in custom_labels:
            deployment_file.write(f'    {label}: {custom_labels.get(label)}\n')
        deployment_file.write(f'    maintainer: "{maintainer}"\n')
        deployment_file.write(f'  name: "{dep_name}-dep"')
        deployment_file.write("""
spec:
  selector:
    matchLabels:
""")
        for selector in dep_template_selectors:
            deployment_file.write(f'      {selector}: {dep_template_selectors.get(selector)}\n')
        deployment_file.write("""
  template:
    metadata:
      labels:
        {{include "hmg.labels" . | indent 8}}
""")

        for selector in dep_template_selectors:
            deployment_file.write(f'        {selector}: {dep_template_selectors.get(selector)}\n')
        deployment_file.write("""
    spec:
      containers:
""")
        for container in containers:
            deployment_file.write(f"""
        - image: "{"{{ .Values.deployments." + dep_name + ".containers." + container["name"] + ".image }}"}:{"{{ .Values." + dep_name + ".containers." + container["name"] + ".imageTag }}"}"
          imagePullPolicy: Always
          name: {container["name"]}
          ports:
            - name: default-port
              containerPort: "{"{{ .Values.deployments." + dep_name + ".containers." + container["name"] + ".port }}"}"
          resources:
            limits:
              cpu: "{"{{ .Values.deployments." + dep_name + ".containers." + container["name"] + ".resources.limits.cpu }}"}"
              memory: "{"{{ .Values.deployments." + dep_name + ".containers." + container["name"] + ".resources.limits.memory }}"}" 
            requests:
              cpu:  "{"{{ .Values.deployments." + dep_name + ".containers." + container["name"] + ".resources.requests.cpu }}"}"
              memory: "{"{{ .Values.deployments." + dep_name + ".containers." + container["name"] + ".resources.requests.memory }}"}" 
""")


    

def _helmy_add_custom_labels():
    custom_labels = {'helmy.hmg.systems/generated': 'yes'}
    labels_unfinished = True
    while labels_unfinished:
        label_name = input('Enter the name of your label, type "done" or leave empty to exit this prompt >')
        if label_name.lower() == 'done' or label_name.lower() == "":
            labels_unfinished = False
        else:
            if label_name.replace('-','').replace('_','').replace('.','').isalnum() and label_name[0] not in ['-', '_', '.'] and label_name[-1] not in ['-', '_', '.']:
                label_value = input('Enter the value of your label >')
                custom_labels[label_name] = label_value
            else:
                print('Label names must be alphanumerical allowed are [a-zA-Z0-9] and dashes (-) underscores (_) and dots (.) inbetween.')
    return custom_labels

def _helmy_create_finish_chart(chart_name, maintainer, values_yaml):

    with open(f'{chart_name}/values.yaml', 'w') as values_file:
        yaml.dump(values_yaml, values_file)
    with open(f'{chart_name}/Chart.yaml', 'w') as chart_file:
        chart_yaml= {
            'apiVersion': 'v2',
            'name': chart_name,
            'description': f'helmy-generated chart for {chart_name}',
            'type': 'application',
            'version': '0.1.0',
            'appVersion': '1.0.0',
            'maintainers':[
                {'name': maintainer}
            ]
        }
        yaml.dump(chart_yaml, chart_file)

    with open(f'{chart_name}/.helmignore', 'w') as helm_ignore:
        helm_ignore.write("""# Patterns to ignore when building packages.
# This supports shell glob matching, relative path matching, and
# negation (prefixed with !). Only one pattern per line.
.DS_Store
# Common VCS dirs
.git/
.gitignore
.bzr/
.bzrignore
.hg/
.hgignore
.svn/
# Common backup files
*.swp
*.bak
*.tmp
*.orig
*~
# Various IDEs
.project
.idea/
*.tmproj
.vscode/
""")
    with open(f'{chart_name}/templates/_helper.tpl', 'w') as helper_file:
        helper_file.write("""{{/* Basic labels with best practice suggested by helm docs */}}
{{ define "hmg.labels" }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.AppVersion }}
app.kubernetes.io/component: {{.Values.instanceName}}
generator: helm
date: {{ now | htmlDate }}
{{ end }}
{{ define "hmg.api.Service" }}
apiVersion: v1
{{ end }}
{{ define "hmg.api.Deployment" }}
apiVersion: apps/v1
{{ end }}
{{ define "hmg.api.StatefulSet" }}
apiVersion: apps/v1
{{ end }}
{{ define "hmg.api.Ingress" }}
apiVersion: networking.k8s.io/v1beta1
{{ end }}""")
    with open(f'{chart_name}/templates/_ingress.tpl', 'w') as ingress_helper:
        ingress_helper.write("""{{ define "hmg.ingressAnnotations" }}
kubernetes.io/ingress.class: nginx
nginx.ingress.kubernetes.io/rewrite-target: /
{{- if eq .Values.ingress.sessionStickyness true }}
nginx.ingress.kubernetes.io/affinity: cookie
nginx.ingress.kubernetes.io/affinity-mode: persistent
nginx.ingress.kubernetes.io/session-cookie-hash: sha1
nginx.ingress.kubernetes.io/session-cookie-name: ing-{{ .Values.instanceName }}-cookie
nginx.ingress.kubernetes.io/proxy-body-size: 21m
{{- end }}
{{- end }}""")
