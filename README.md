# helmy module

## Setup

You need to have `helm` installed for helmy to work

## Configuration

## Commands

|Name|Description|Example Parameters|
|---|---|---|
|`lint`|Lint a helm chart|-|
|`lint-ignore`|Create a helmy.yaml in the current folder|-|
|`create`|Create a helm chart interactively|-|
|`explain`|Explain a helm chart|-|

### Lint

`helmy lint` will check your helm charts for errors and missing fields. For helmy to find your helm chart, you need to be in the _root directory_ of your helm chart (the directory with the Chart.yaml).

When helmy finds your helm chart it will first check it for basic errors through `helm lint` and then use `helm template` to render your template for further linting. If it encounters an error it will stop the process and tell you the Object and the error it encountered.
You can tell helmy to ignore errors either with a `helmy.yaml` in your helm chart _root directory_ or with kubernets inline labels.

#### helmy.yaml

Bundled in this repo is a commented `helmy.yaml`. It is configured to use the same settings as `helmy` uses per default (exit on any error). If you want to ignore a specific check for every object in your helm chart, set the field to `true`.

You can also use `helmy lint-ignore` to create a `helmy.yaml` in the current directory. The created file uses the default configuration and must be edited for helmy to ignore specific errors.

#### Ignore by inline labels

Instead of disabling a specific check for every object, you can also use inline labels to ignore a check. The syntax for ignoring a check by label is `helmy.hmg.systems/ignore-<category>.<checkname>`. For example we have an ingress template and want to ignore the `hostMismatch` check:

```yaml
  apiVersion: networking.k8s.io/v1beta1
  kind: Ingress
  metadata:
    labels:
      helmy.hmg.systems/ignore-ingress.hostMismatch: true
    annotations:
      kubernetes.io/ingress.class: nginx
      ...
```

This would ignore the hostMismatch check for this ingress object only.
